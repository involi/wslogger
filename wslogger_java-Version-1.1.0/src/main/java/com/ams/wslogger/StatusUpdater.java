package com.ams.wslogger;

public interface StatusUpdater {

    /**
     * Set the status text/line of the implementer.
     * 
     * @param status status line to be shown
     */
    void setStatus(String status);

    /**
     * Set the progress bar value to the given <code>value</code>.
     * 
     * @param value progress var value
     */
    void setProgressValue(int value);

    /**
     * Enable or disable the controls (may be buttons and visibility of other
     * components).
     * 
     * @param value value flag
     */
    void enableControls(boolean value);

    /**
     * Show or hide status showing components.
     * 
     * @param value value flag
     */
    void showStatusComponents(boolean value);

    /**
     * Show an error message dialog.
     * 
     * @param msg message to show
     */
    void showErrorMsg(String msg);
}