package com.ams.wslogger;

public interface WorkerHandler {

    /**
     * Asks the worker to open connection
     * 
     * @param timeLap time lap while the worker should be running/working.
     */
    void openConnection(long timeLap);

    /**
     * Asks the worker to close connection
     */
    void closeConnection();
}