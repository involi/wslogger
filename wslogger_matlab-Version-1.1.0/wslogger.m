function wslogger
    clean_old_version;
    installed = check_version_110;
    if ~installed
        installed = install_version_110;
        if ~installed
            msgbox('Could not install Version 1.1.0', 'Error');
        end       
    end
    if ~is_path_set
        msgbox({"Installed Version 1.1.0",...
            "Please add the following line to your",...
            "system's environmental path variable",...
            "and sign-out, sign-in your system, then"...
            "restart matlab."...
            ""...
            fullfile(matlabroot,'bin',computer('arch'))
            }, 'WSLogger');
        return;
    end
    % start an instance of the gui
    cmd = "start " + ...
        "/b " + ...
        "java " + ...
        "-jar """ + ...
        strcat(prefdir, filesep, 'wslogger-1.1.0', filesep, 'wslogger-1.1.0.jar') + ...
        """";
    system(cmd);
end

%
% check if the env path is set
%
function found = is_path_set
    tgtPath = fullfile(matlabroot,'bin',computer('arch'));
    pathCell = regexp(getenv('path'), pathsep, 'split');
    if ispc
        found = any(strcmpi(pathCell, tgtPath));
    else
        found = any(strcmp(pathCell, tgtPath));
    end
end

%
% function to check if the old version is installed. this will clean it
%
function found = clean_old_version
    jar_file_name = 'matlab-websocket-1.4.jar';
    found = false;
    if exist(strcat(prefdir, filesep, jar_file_name), 'file') == 2
        fprintf('Old version detected. Cleaning up...\n')
        found = true;
        delete(strcat(prefdir, filesep, jar_file_name));
        % additionally check for classpath.txt if its there delete that
        % too
        if exist(strcat(prefdir, filesep, 'classpath.txt'), 'file') == 2
            delete(strcat(prefdir, filesep, 'classpath.txt'));
        end
    end
end

%
% check for version 1.0.0 installation
%
function installed = check_version_100
    installed = false;
    if exist(strcat(prefdir, filesep, 'wslogger-1.0.0'), 'dir') == 7
        installed = true;
    end
end

%
% install version 1.0.0
%
function installed = install_version_100
    installed = false;
    fprintf('Installing WSLogger v1.0.0...\n');
    curDir = fileparts(which('wslogger'));
    if exist(strcat(curDir, filesep, 'wslogger-1.0.0'), 'dir') ~= 7
        msgbox('Could not locate data folder for version 1.0.0', 'WSLogger');
        return;
    end
    copyfile(strcat(curDir, filesep, 'wslogger-1.0.0'), strcat(prefdir, filesep, 'wslogger-1.0.0'));
    installed = true;
end

%
% check for version 1.1.0 installation
%
function installed = check_version_110
    installed = false;
    if exist(strcat(prefdir, filesep, 'wslogger-1.1.0'), 'dir') == 7
        installed = true;
    end
end

%
% install version 1.1.0
%
function installed = install_version_110
    installed = false;
    fprintf('Installing WSLogger v1.1.0...\n');
    curDir = fileparts(which('wslogger'));
    if exist(strcat(curDir, filesep, 'wslogger-1.1.0'), 'dir') ~= 7
        msgbox('Could not locate data folder for version 1.1.0', 'WSLogger');
        return;
    end
    copyfile(strcat(curDir, filesep, 'wslogger-1.1.0'), strcat(prefdir, filesep, 'wslogger-1.1.0'));
    installed = true;
end
