package com.ams.wslogger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingWorker;

import com.mathworks.engine.MatlabEngine;

import org.java_websocket.handshake.ServerHandshake;

class Worker extends SwingWorker<Void, StatusUpdate> implements WSListener {

    private WorkerState workerState = WorkerState.Starting;
    private StatusUpdater parent = null;
    private MatlabEngine engine = null;
    private WSClient client = null;
    private OutputNameFactory oNameFactory = new OutputNameFactory();
    private long timeLap = 0;
    private StopWatch watch = new StopWatch();
    private List<BaseWriter> writers = new ArrayList<>();
    private Logger logger = Logger.getLogger(Worker.class.getName());

    public Worker() {
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(StatusUpdater parent) {
        this.parent = parent;
    }

    /**
     * @param outputDir the outputDir to set
     */
    public void setOutputDir(String outputDir) {
        oNameFactory.setOutDir(outputDir);
    }

    /**
     * Starts a new job with the provided time lap.
     * 
     * @param timeLap time lap
     */
    public void startJob(long timeLap) {
        if (workerState == WorkerState.Idle) { // only accept new job when idle
            this.timeLap = timeLap;
            workerState = WorkerState.PreparingForNewJob; // set the state to preparing stage
                                                          // so that the worker can prepare itself before starting
                                                          // collecting data
        }
    }

    /**
     * Stops currently running job
     */
    public void stopJob() {
        WorkerState prev = workerState;
        if (workerState == WorkerState.Working) { // only accept a stop signal when working
            workerState = WorkerState.StopRequested;
        }
        logger.log(Level.INFO, "Stop Requested. PREV: " + prev + " CUR: " + workerState);
    }

    /**
     * @return the outputDir
     */
    public String getOutputDir() {
        return oNameFactory.getOutDir();
    }

    /**
     * @return the workerState
     */
    public WorkerState getWorkerState() {
        return workerState;
    }

    /**
     * @return the parent
     */
    public StatusUpdater getParent() {
        return parent;
    }

    @Override
    public void onOpen(ServerHandshake handshake) {
        logger.info("Connection opened!");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        logger.info("Connection closed! Code: " + code + " Reason: " + reason + " Remote: " + remote);
    }

    @Override
    public void onError(Exception ex) {
        logger.log(Level.SEVERE, ex.getMessage());
    }

    @Override
    public void onMessage(String message) {
        if (!writers.isEmpty()) {
            Date now = new Date();
            for (BaseWriter writer : writers) {
                writer.push(new WSData(now, message));
            }
        }
    }

    /**
     * Opens a brand new websocket connection and sets the internal client variable
     * to the new client instance.
     * 
     * @throws Exception
     */
    protected boolean openConnection() throws Exception {
        // clients are not reusable
        if (client != null) {
            return false;
        }

        // need to open a new client every time
        try {
            client = new WSClient();
            client.setListener(this);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            client = null;
            // workerState = WorkerState.Idle; // put worker in idle mode
            publishStatus(new StatusUpdate(WorkerState.Idle, 0, "Failed to create client!"));
            return false;
        }

        // try to connect few times if fails
        if (!client.connectBlocking()) {
            client = null;
            publishStatus(new StatusUpdate(WorkerState.Idle, 0, "Failed to connect to server!"));
            return false;
        }

        return true;
    }

    /**
     * Closes any opened connection.
     * 
     * @throws Exception
     */
    protected void closeConnection() throws Exception {
        // if no client is loaded then nothing to do
        if (client == null) {
            return;
        }

        int tryCnt = 0;
        for (; tryCnt < 5; tryCnt++) {
            client.closeBlocking();
            if (client.isClosed()) {
                // nullify client
                client = null;
                break;
            }
        }
        if (tryCnt == 5) {
            logger.log(Level.SEVERE, "Connection close failed after " + tryCnt + " times!");
            workerState = WorkerState.Idle; // put worker in idle mode if fails
        }
    }

    /**
     * Start writer threads
     */
    private void startWriters() {
        // set the start time of the name factory
        oNameFactory.setStart(new Date());

        MatlabWriter matlabWriter = new MatlabWriter();
        matlabWriter.setNameFactory(oNameFactory);
        matlabWriter.setEngine(engine);
        writers.add(matlabWriter);
        matlabWriter.start();

        CsvWriter csvWriter = new CsvWriter();
        csvWriter.setNameFactory(oNameFactory);
        writers.add(csvWriter);
        csvWriter.start();
    }

    /**
     * Wait for the writers to finish up.
     */
    private void waitForWriters() {
        // set end time of name factory
        oNameFactory.setEnd(new Date());

        for (BaseWriter writer : writers) {
            try {
                writer.setNoMoreData(true);
                writer.join();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }
        // remove old threads
        writers.clear();
    }

    @Override
    protected Void doInBackground() throws Exception {
        logger.info("Worker started....");
        publishStatus(new StatusUpdate(WorkerState.Starting, 0, "Loading. Please wait..."));

        // start matlab session
        if (engine == null) {
            try {
                logger.log(Level.INFO, "Trying to connect to existing matlab session");
                String[] sessions = MatlabEngine.findMatlab();
                String found = "";
                for (String session : sessions) {
                    found += session + ",";
                }
                logger.log(Level.INFO, "Found session: " + found);
                if (sessions.length > 0) {
                    engine = MatlabEngine.connectMatlab(sessions[0]);
                    logger.log(Level.INFO, "Connected to existing matlab session");
                } else {
                    throw new RuntimeException("No running session found!");
                }
            } catch (Exception ex) {
                logger.log(Level.WARNING, ex.getMessage());
                logger.log(Level.INFO, "Trying to start a new matlab session");
                try {
                    engine = MatlabEngine.startMatlab();
                    logger.log(Level.INFO, "Started a new matlab session");
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage());
                    engine = null;
                }
            }
        }

        // set the status message to "Ready"
        publishStatus(new StatusUpdate(WorkerState.Started, 0, "Started Matlab session..."));
        // wait to show it on GUI
        Thread.sleep(1000);
        // set the status message to "Idle"
        publishStatus(new StatusUpdate(WorkerState.Idle, 0, "Ready for new job..."));

        while (!isCancelled()) {
            switch (workerState) {
            case PreparingForNewJob: { // new job arrived
                publishStatus(new StatusUpdate(WorkerState.PreparingForNewJob, 0, "Preparing..."));
                parent.enableControls(false);
                // start data writing threads
                startWriters();
                // wait a while to allow other threads to start
                Thread.sleep(100);
                if (!openConnection()) {
                    publishStatus(new StatusUpdate(WorkerState.SocketError, 0, "Failed to connect to server!"));
                    break;
                }
                publishStatus(new StatusUpdate(WorkerState.Prepared, 0, "Prepared..."));
                watch.reset();
                watch.start();
                publishStatus(new StatusUpdate(WorkerState.Working, 0, "Collecting Data..."));
            }
                break;
            case Working: {
                // update progress
                long spent = watch.timeSpent();
                int percent = (int) ((spent * 100L) / timeLap);
                publishStatus(new StatusUpdate(WorkerState.Working, percent,
                        "Collecting Data (" + Integer.toString(percent) + "%)..."));
                // check elapsed time
                if (spent >= timeLap) {
                    publishStatus(new StatusUpdate(WorkerState.StopRequested, 0, "Finishing up. Please wait..."));
                    break;
                }
            }
                break;
            case SocketError: {
                Thread.sleep(100);
                publishStatus(new StatusUpdate(WorkerState.StopRequested, 0, "Connection Error!"));
                Thread.sleep(100);
            }
                break;
            case StopRequested: {
                publishStatus(new StatusUpdate(WorkerState.Stopping, 0, "Closing connection..."));
                closeConnection();
                watch.stop();
                publishStatus(new StatusUpdate(WorkerState.Stopping, 0, "Finishing up. Please wait..."));
                // wait for data writers to finish
                waitForWriters();
                publishStatus(new StatusUpdate(WorkerState.Stopped, 0, "Stopped collecting data!"));
                Thread.sleep(1000);
                publishStatus(new StatusUpdate(WorkerState.Idle, 0, "Ready for new job..."));
            }
                break;
            default:
                break;
            }

            // give it a break anyway
            Thread.sleep(10);
        }

        // put the state into stopping stage
        publishStatus(new StatusUpdate(WorkerState.Stopping, 0, "Cleaning up. Please wait..."));

        // check if the connection is stil alive, if so try to close it
        if (client != null && !client.isClosed()) {
            closeConnection();
        }

        // close writer threads
        if (!writers.isEmpty()) {
            waitForWriters();
        }

        // close matlab session
        if (engine != null) {
            engine.close();
        }

        // put the state into stopped stage
        publishStatus(new StatusUpdate(WorkerState.Stopped, 0, "Worker Stopped !"));

        logger.info("Worker Stopped...");
        return null;
    }

    /**
     * Proxy the parents publish method to allow setting worker state in one call.
     * 
     * @param statusUpdates new status update
     */
    protected void publishStatus(StatusUpdate statusUpdates) {
        publish(statusUpdates);
        workerState = statusUpdates.state;
    }

    @Override
    protected void process(List<StatusUpdate> statusList) {
        if (!statusList.isEmpty()) {
            StatusUpdate status = statusList.get(statusList.size() - 1);

            parent.setStatus(status.message);

            if (status.state != WorkerState.Idle) { // disable the controls
                parent.enableControls(false);
            } else { // otherwise enable the controls
                parent.enableControls(true);
            }

            if (status.state == WorkerState.Working) { // show the progress bar
                parent.showStatusComponents(true);
                parent.setProgressValue(status.progress);
            } else {
                parent.showStatusComponents(false);
            }

            // show the server/socket error
            if (status.state == WorkerState.SocketError) {
                parent.showErrorMsg(status.message);
            }

            // also remove it from the list, so that it doesn't come up again
            statusList.remove(status);
        }
    }
}