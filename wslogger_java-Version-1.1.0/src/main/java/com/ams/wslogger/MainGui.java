package com.ams.wslogger;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

/**
 * Main GUI window.
 */
public class MainGui extends JFrame implements StatusUpdater, WorkerHandler {

    static final long serialVersionUID = 1;

    static final String version = "1.1.0";

    // wesocket server link edit
    JTextField serverLinkField = new JTextField(29);

    // output directory related components and vars
    JButton browseBtn = new JButton("Browse");
    JTextField outDirField = new JTextField(18);
    String initOutDir = System.getProperty("user.home"); // default output directory
    OutputNameFactory outputNameFactory = new OutputNameFactory();

    // timer parameters
    JSpinner hourField = new JSpinner(new SpinnerNumberModel(0, 0, null, 1));
    JSpinner minField = new JSpinner(new SpinnerNumberModel(0, 0, null, 1));
    JSpinner secField = new JSpinner(new SpinnerNumberModel(0, 0, null, 10));

    // logo size
    final int LogoSize = 170;

    // command/control components and vars
    JButton startBtn = new JButton("Start");
    JButton stopBtn = new JButton("Stop");
    JProgressBar progressBar = new JProgressBar();
    JLabel statusLbl = new JLabel();

    // use logger instead of system out
    private Logger logger = Logger.getLogger(MainGui.class.getName());

    // worker thread
    Worker worker = new Worker();

    /**
     * Constructor to start GUI with default users home directory set as initial
     * output directory.
     */
    public MainGui() {
        this(null);
    }

    /**
     * Sets the initial output directory to the <code>workingPath</code> if not
     * <code>null</code>
     * 
     * @param workingPath initial working/output directory
     */
    public MainGui(String workingPath) {
        super();

        // set output directory if provided
        if (workingPath != null && !workingPath.isEmpty()) {
            initOutDir = workingPath;
        }

        // initialize window layout and components
        initUI();

        // initialize various action/event listeners
        initListeners();

        // make the controls disabled at beginning
        enableControls(false);

        // dont show the status components either
        showStatusComponents(false);

        // init worker parent and start running
        worker.setParent(this);
        worker.execute();
    }

    /**
     * Initializes various action/event listeners.
     */
    protected void initListeners() {
        // exit button clicked event -> close/dispose the window
        addWindowListener(new WindowListener() {

            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                // close the worker thread
                // this will also try to close opened connection and flush remainging data
                worker.cancel(false);
                try {
                    // wait to close
                    worker.get();
                } catch (Exception exc) {
                    logger.log(Level.SEVERE, "Error closing worker: " + exc.getMessage());
                }

                // handle closing activities
                e.getWindow().dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }
        });

        // add output directory selection action
        Component self = this;
        browseBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String outDir = DirectoryChooser.getOutputDirectory(self, "Select Output Directory",
                        outDirField.getText());
                if (outDir != null && !outDir.isEmpty()) {
                    outDirField.setText(outDir);
                    worker.setOutputDir(outDir);
                }
            }
        });

        // start button action listener
        startBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // ask the user if the timer setting is correct or not
                long seconds = ((Number) secField.getValue()).longValue()
                        + 60L * ((Number) minField.getValue()).longValue()
                        + 60L * 60L * ((Number) hourField.getValue()).longValue();
                if (seconds <= 0) { // nothing to do if duration for timer is 0 or less
                    return;
                }
                String url = serverLinkField.getText();
                url = url.isEmpty() ? WSClient.getURL() : url.trim();
                String msg = "<html>Start the timer for <br>"
                        + Long.toString(((Number) hourField.getValue()).longValue()) + " hours, "
                        + Long.toString(((Number) minField.getValue()).longValue()) + " minutes and "
                        + Long.toString(((Number) secField.getValue()).longValue()) + " seconds ( "
                        + Long.toString(seconds) + " seconds in total ) <br> with URL set to <br>" + url + " ?</html>";
                int choice = JOptionPane.showConfirmDialog(self, msg, "Confirm", JOptionPane.OK_CANCEL_OPTION);

                if (choice == JOptionPane.YES_OPTION) {
                    // set the server url (static) before starting up
                    WSClient.setURL(url);
                    serverLinkField.setText(url);
                    logger.log(Level.INFO, "Server URL set to " + url);
                    // make sure we are using the latest output directory
                    worker.setOutputDir(outDirField.getText());
                    // ask worker to open/reopen connection and start collecting data
                    openConnection(seconds);
                }
            }
        });

        // stop button action listener
        stopBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // ask the worker to stop if its running
                closeConnection();
            }
        });
    }

    /**
     * Resize an image to a new size
     * 
     * @param srcImage source image
     * @param w        new width
     * @param h        new height
     * @return new resized image
     */
    private Image resizeImage(Image srcImage, int w, int h) {
        BufferedImage outImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = outImage.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImage, 0, 0, w, h, null);
        g2.dispose();

        return outImage;
    }

    /**
     * Initialize the UI components and layouts.
     */
    protected void initUI() {
        // set properties of the main ui
        setResizable(false);
        setTitle("WebSocket Logger v" + MainGui.version);
        ImageIcon icon = new ImageIcon(getClass().getResource("/images/logo.png"));
        setIconImage(icon.getImage());

        // get some fonts
        Font boldFont = new Font(Font.SANS_SERIF, Font.BOLD, 16);
        Font regularFont = new Font(Font.SANS_SERIF, Font.PLAIN, 16);
        try {
            InputStream boldFontStream = getClass().getResourceAsStream("/fonts/Lato-Semibold.ttf");
            InputStream regularFontStream = getClass().getResourceAsStream("/fonts/Lato-Regular.ttf");
            boldFont = Font.createFont(Font.TRUETYPE_FONT, boldFontStream);
            regularFont = Font.createFont(Font.TRUETYPE_FONT, regularFontStream);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error loading fonts: " + e.getMessage());
        }
        Font titleFont = boldFont.deriveFont(16f);
        // these 2 fonts must have same size
        Font btnFont = boldFont.deriveFont(14f);
        Font lblFont = regularFont.deriveFont(14f);

        /*********************************
         * select output directory panel
         *********************************/
        JLabel outDirPromptLbl = new JLabel("Select output directory - ");
        outDirPromptLbl.setFont(lblFont);
        browseBtn.setFont(btnFont);
        // set outDirField size consistent with the button height
        outDirField.setMaximumSize(
                new Dimension(outDirField.getPreferredSize().width, browseBtn.getPreferredSize().height));
        outDirField.setFont(lblFont);
        // show initial value
        outDirField.setText(initOutDir);

        JPanel outDirPanel = new JPanel();
        GroupLayout outDirPL = new GroupLayout(outDirPanel);
        outDirPanel.setLayout(outDirPL);
        TitledBorder outDirBorder = BorderFactory.createTitledBorder("Step 1");
        outDirBorder.setTitleFont(titleFont);
        outDirPanel.setBorder(outDirBorder);
        outDirPL.setAutoCreateGaps(true);
        outDirPL.setAutoCreateContainerGaps(true);

        // output directory panel layout
        outDirPL.setHorizontalGroup(outDirPL.createParallelGroup().addComponent(outDirPromptLbl).addGap(20)
                .addGroup(outDirPL.createSequentialGroup().addComponent(browseBtn).addComponent(outDirField)));
        outDirPL.setVerticalGroup(outDirPL.createSequentialGroup().addComponent(outDirPromptLbl).addGap(20)
                .addGroup(outDirPL.createParallelGroup().addComponent(browseBtn).addComponent(outDirField)));

        /**********************************
         * setup the time parameter panel
         **********************************/
        hourField.setFont(lblFont);
        minField.setFont(lblFont);
        secField.setFont(lblFont);

        JLabel timerPPromptLbl = new JLabel("Set timer parameters -");
        JLabel hourLbl = new JLabel("Hour:");
        JLabel minLbl = new JLabel("Minute:");
        JLabel secLbl = new JLabel("Second:");
        timerPPromptLbl.setFont(lblFont);
        hourLbl.setFont(lblFont);
        minLbl.setFont(lblFont);
        secLbl.setFont(lblFont);
        // make sure the labels have the same height as the input fields
        hourLbl.setMaximumSize(new Dimension(hourLbl.getPreferredSize().width, hourField.getPreferredSize().height));
        minLbl.setMaximumSize(new Dimension(minLbl.getPreferredSize().width, minField.getPreferredSize().height));
        secLbl.setMaximumSize(new Dimension(secLbl.getPreferredSize().width, secField.getPreferredSize().height));

        // create the groupbox panel
        JPanel timerPPanel = new JPanel();
        GroupLayout timerPGL = new GroupLayout(timerPPanel);
        timerPPanel.setLayout(timerPGL);
        TitledBorder timerPBorder = BorderFactory.createTitledBorder("Step 2");
        timerPBorder.setTitleFont(titleFont);
        timerPPanel.setBorder(timerPBorder);
        timerPGL.setAutoCreateGaps(true);
        timerPGL.setAutoCreateContainerGaps(true);

        // make sure both panels are equal sized
        timerPPanel.setMaximumSize(
                new Dimension(outDirPanel.getPreferredSize().width, timerPPanel.getPreferredSize().height));

        // add timer panel components
        timerPGL.setHorizontalGroup(timerPGL.createParallelGroup().addComponent(timerPPromptLbl).addGap(20)
                .addGroup(timerPGL.createSequentialGroup()
                        .addGroup(timerPGL.createParallelGroup().addComponent(hourLbl).addComponent(minLbl)
                                .addComponent(secLbl))
                        .addGroup(timerPGL.createParallelGroup().addComponent(hourField).addComponent(minField)
                                .addComponent(secField))));

        timerPGL.setVerticalGroup(timerPGL.createSequentialGroup().addComponent(timerPPromptLbl).addGap(20)
                .addGroup(timerPGL.createParallelGroup()
                        .addGroup(timerPGL.createSequentialGroup().addComponent(hourLbl).addComponent(minLbl)
                                .addComponent(secLbl))
                        .addGroup(timerPGL.createSequentialGroup().addComponent(hourField).addComponent(minField)
                                .addComponent(secField))));

        /**********************************
         * Show the logo on right side
         **********************************/
        JLabel logoLbl = new JLabel();
        logoLbl.setPreferredSize(new Dimension(LogoSize, LogoSize));
        logoLbl.setMaximumSize(logoLbl.getPreferredSize());
        logoLbl.setIcon(new ImageIcon(resizeImage(icon.getImage(), LogoSize, LogoSize)));

        /**********************************
         * Control button panel
         **********************************/
        JPanel cmdPanel = new JPanel();
        GroupLayout cmdL = new GroupLayout(cmdPanel);
        cmdPanel.setLayout(cmdL);
        TitledBorder cmdBorder = BorderFactory.createTitledBorder("Step 3");
        cmdBorder.setTitleFont(titleFont);
        cmdPanel.setBorder(cmdBorder);
        cmdL.setAutoCreateGaps(true);
        cmdL.setAutoCreateContainerGaps(true);

        startBtn.setFont(btnFont);
        startBtn.setMaximumSize(new Dimension(LogoSize - 30, startBtn.getPreferredSize().height));
        stopBtn.setFont(btnFont);
        stopBtn.setMaximumSize(new Dimension(LogoSize - 30, stopBtn.getPreferredSize().height));
        // progressBar.setMaximumSize(new
        // Dimension(progressBar.getPreferredSize().width, 20));
        // make start button inactive at beginning

        cmdL.setHorizontalGroup(cmdL.createParallelGroup().addComponent(startBtn).addComponent(stopBtn).addGap(19));
        cmdL.setVerticalGroup(cmdL.createSequentialGroup().addComponent(startBtn).addComponent(stopBtn).addGap(19));

        /************************************
         * status bar and progress bar setup
         ************************************/
        statusLbl.setFont(lblFont);
        setStatus("Loading. Please Wait...");
        Dimension statusDim = new Dimension(250, statusLbl.getPreferredSize().height);
        statusLbl.setMaximumSize(statusDim);
        progressBar.setMaximumSize(statusDim);

        JPanel statusPanel = new JPanel();
        GroupLayout statusPanelL = new GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelL);

        statusPanelL.setHorizontalGroup(
                statusPanelL.createSequentialGroup().addComponent(statusLbl).addComponent(progressBar));
        statusPanelL
                .setVerticalGroup(statusPanelL.createParallelGroup().addComponent(statusLbl).addComponent(progressBar));

        /**********************************
         * Server link edit panel
         **********************************/
        JLabel serverLinkLbl = new JLabel("Server URL: ");
        serverLinkLbl.setFont(lblFont);
        serverLinkField.setFont(lblFont);
        serverLinkField.setText(WSClient.getURL()); // set initial link to v3 link
        serverLinkLbl.setMaximumSize(
                new Dimension(serverLinkLbl.getPreferredSize().width, serverLinkField.getPreferredSize().height));

        JPanel serverLinkPanel = new JPanel();
        GroupLayout serverLinkL = new GroupLayout(serverLinkPanel);
        serverLinkPanel.setLayout(serverLinkL);

        serverLinkL.setAutoCreateContainerGaps(true);
        serverLinkL.setAutoCreateGaps(true);

        serverLinkL.setHorizontalGroup(
                serverLinkL.createSequentialGroup().addComponent(serverLinkLbl).addComponent(serverLinkField));
        serverLinkL.setVerticalGroup(
                serverLinkL.createParallelGroup().addComponent(serverLinkLbl).addComponent(serverLinkField));

        /**********************************
         * add components to the main ui layout
         **********************************/
        Container mainPane = getContentPane();
        GroupLayout mL = new GroupLayout(mainPane);
        mainPane.setLayout(mL);

        mL.setAutoCreateGaps(true);
        mL.setAutoCreateContainerGaps(true);

        mL.setHorizontalGroup(mL.createParallelGroup().addComponent(serverLinkPanel)
                .addGroup(mL.createSequentialGroup()
                        .addGroup(mL.createParallelGroup().addComponent(outDirPanel).addComponent(timerPPanel))
                        .addGroup(mL.createParallelGroup().addComponent(logoLbl).addComponent(cmdPanel)))
                .addComponent(statusPanel));
        mL.setVerticalGroup(mL.createSequentialGroup().addComponent(serverLinkPanel)
                .addGroup(mL.createParallelGroup()
                        .addGroup(mL.createSequentialGroup().addComponent(outDirPanel).addComponent(timerPPanel))
                        .addGroup(mL.createSequentialGroup().addComponent(logoLbl).addComponent(cmdPanel)))
                .addComponent(statusPanel));

        // pack the components
        pack();
        setLocationRelativeTo(null); // centerize the gui
    }

    /**
     * Set the current status text
     * 
     * @param status text to show as current status
     */
    @Override
    public void setStatus(String status) {
        statusLbl.setText("Status: " + status);
    }

    /**
     * Set the currect progress bar value
     * 
     * @param value value to be set
     */
    @Override
    public void setProgressValue(int value) {
        progressBar.setValue(value);
    }

    /**
     * Enables/Disables controls and status showing components.
     * 
     * @param value <code>true</code> to enable/show the controls.
     */
    @Override
    public void enableControls(boolean value) {
        browseBtn.setEnabled(value);
        startBtn.setEnabled(value);
        outDirField.setEnabled(value);
        serverLinkField.setEnabled(value);
    }

    /**
     * Show or hide the progress bar
     * 
     * @param value <code>true</code> to show the bar
     */
    @Override
    public void showStatusComponents(boolean value) {
        progressBar.setVisible(value);
    }

    /**
     * This method allows the worker to show arbitrary error message
     * 
     * @param msg message to show
     */
    @Override
    public void showErrorMsg(String msg) {
        try {
            Component self = this;
            JOptionPane.showConfirmDialog(self, msg, "Error", JOptionPane.OK_OPTION);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    /**
     * Request the worker to start collecting data
     */
    @Override
    public void openConnection(long timeLap) {
        // request the worker to start a new job
        worker.startJob(timeLap);
    }

    /**
     * Request the worker to close connection and flush remaining data
     */
    @Override
    public void closeConnection() {
        // request the worker to stop if running
        worker.stopJob();
    }
}