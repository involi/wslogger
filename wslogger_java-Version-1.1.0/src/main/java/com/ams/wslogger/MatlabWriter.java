package com.ams.wslogger;

import java.util.logging.Level;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

import com.mathworks.engine.MatlabEngine;

public class MatlabWriter extends BaseWriter {
    MatlabEngine engine = null;

    /**
     * Set the Matlab session object.
     * 
     * @param engine the engine to set
     */
    public void setEngine(MatlabEngine engine) {
        this.engine = engine;
    }

    /**
     * Get the current matlab session object.
     * 
     * @return the engine
     */
    public MatlabEngine getEngine() {
        return engine;
    }

    @Override
    public boolean openFile() {
        return true;
    }

    @Override
    public void closeFile() {
        // empty method
    }

    @Override
    public void writeData(WSData wsData, long index) {
        // write a single string of data marked by some index
        String varName = "D_" + Long.toString(index);

        if (engine == null) { // no engine no save to mat functionality
            return;
        }

        try {
            engine.putVariable(varName, 0);
            String cmd = varName + " = struct('timestamp_local', '" + wsData.timestampLocalStr
                    + "', 'data', jsondecode('" + wsData.data + "'));";
            engine.eval(cmd);
            String tmpFilePath = nameFactory.getTmpMatFileName();
            File tmpFile = new File(tmpFilePath);
            if (tmpFile.exists()) {
                cmd = "save('" + tmpFilePath + "', '" + varName + "', '-append');";
            } else {
                // cmd = "save('" + tmpFilePath + "', '" + varName + "', '-v7.3');";
                cmd = "save('" + tmpFilePath + "', '" + varName + "');";
            }
            engine.eval(cmd);
            cmd = "clear " + varName + ";";
            engine.eval(cmd);

            // String tmpFilePath = nameFactory.getTmpMatFileName();
            // File tmpFile = new File(tmpFilePath);
            // if (!tmpFile.exists()) {
            // // save it for the first time with v7.3 flag and also load a workspace
            // variable
            // // with the matfile
            // engine.putVariable("Data", 0);
            // engine.eval("Data = [];");
            // String cmd = "Data = [Data, jsondecode('" + data + "')];";
            // engine.eval(cmd);
            // cmd = "save('" + tmpFilePath + "', 'Data', '-v7.3', '-nocompression');";
            // engine.eval(cmd);

            // engine.putVariable("matFile", 0);
            // engine.eval("matFile = matfile('" + tmpFilePath + "', 'Writable', true);");
            // } else {
            // String cmd = "matFile.Data(length(matFile.Data)+1, 1) = jsondecode('" + data
            // + "');";
            // engine.eval(cmd);
            // }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    /**
     * This method basically renames the temporary file to the specific file.
     */
    @Override
    public void removeTemporaryFile() {
        if (Files.exists(Paths.get(nameFactory.getTmpMatFileName()), LinkOption.NOFOLLOW_LINKS)) {
            if ((new File(nameFactory.getTmpMatFileName())).length() > 0) {
                try {
                    Files.move(Paths.get(nameFactory.getTmpMatFileName()),
                            Paths.get(nameFactory.getMatOutputFileName(nameFactory.getStart(), nameFactory.getEnd())));
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, ex.getMessage());
                }
            } else { // delete only for an empty file
                try {
                    Files.deleteIfExists(Paths.get(nameFactory.getTmpMatFileName()));
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage());
                }
            }
        }
    }
}