package com.ams.wslogger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

class OutputNameFactory {
    private Date start, end;
    private String outDir;
    private String ext;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");

    /**
     * @param start the start date/time to set
     */
    public void setStart(Date start) {
        this.start = start;
    }

    /**
     * @param end the end date/time to set
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * @param outDir the output directory to set
     */
    public void setOutDir(String outDir) {
        this.outDir = outDir;
    }

    /**
     * @param ext the output file extension to set
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @return the end
     */
    public Date getEnd() {
        return end;
    }

    /**
     * @return the output directory
     */
    public String getOutDir() {
        return outDir;
    }

    /**
     * @return the output file extension
     */
    public String getExt() {
        return ext;
    }

    /**
     * Constructs a file name from the given start and end datetime, output
     * directory and file extension. Filename is constructed as -
     * 
     * <pre>
     * "01.03.2009_15.23.45__01.03.2009_12.14.55.ext"
     * </pre>
     * 
     * First part of the path is date, then time and start datetime to end datetime.
     * 
     * @param start  start date time
     * @param end    end date time
     * @param outDir output directory
     * @param ext    file extension to be used(without dot)
     * @return the absolute path(even the output directory was in relative path)
     */
    public String getOutputFileName(Date start, Date end, String outDir, String ext) {
        Path outFilePath = Paths.get(outDir);
        // make sure the output directory is absolute path
        if (!outFilePath.isAbsolute()) {
            outFilePath = Paths.get(outDir).toAbsolutePath();
        }

        // formatted dated file name
        String fileName = formatter.format(start) + "__" + formatter.format(end);
        if (ext.startsWith(".")) {
            ext = ext.replaceFirst(".", "");
        }
        fileName += "." + ext;

        // join the output directory and file name
        outFilePath = outFilePath.resolve(fileName);

        return outFilePath.toString();
    }

    /**
     * Get output file name as absolute path string. This method acts same as the
     * <code>getOutputFileName</code> with <code>outDir</code> argument. It expects
     * the <code>outDir<code> property is set prior to this call.
     * 
     * @param start start time
     * @param end   end time
     * @param ext   extension
     * @return output file path
     */
    public String getOutputFileName(Date start, Date end, String ext) {
        return getOutputFileName(start, end, outDir, ext);
    }

    /**
     * Get CSV format output file name.
     * 
     * @param start start time
     * @param end   end time
     * @return absolute output file path
     */
    public String getCsvOutputFileName(Date start, Date end) {
        return getOutputFileName(start, end, outDir, "csv");
    }

    /**
     * Get MAT format output file name.
     * 
     * @param start start time
     * @param end   end time
     * @return absolute output file path
     */
    public String getMatOutputFileName(Date start, Date end) {
        return getOutputFileName(start, end, outDir, "mat");
    }

    /**
     * Get a temporary file path as absolute path.
     * 
     * @param outDir output directory for the temporary file
     * @param ext    extension
     * @return absolute path
     */
    public String getTmpFileName(String outDir, String ext) {
        Path outFilePath = Paths.get(outDir);
        // make sure the output directory is absolute path
        if (!outFilePath.isAbsolute()) {
            outFilePath = Paths.get(outDir).toAbsolutePath();
        }

        // contruct base filename
        String fileName = "__tmp__";
        if (ext.startsWith(".")) {
            ext = ext.replaceFirst(".", "");
        }
        fileName += "." + ext;

        // join the output directory and file name
        outFilePath = outFilePath.resolve(fileName);

        return outFilePath.toString();
    }

    /**
     * Get temporary CSV format file name.
     * 
     * @return absolute path
     */
    public String getTmpCsvFileName() {
        return getTmpFileName(outDir, "csv");
    }

    /**
     * Get temporary MAT format file name.
     * 
     * @return absolute path.
     */
    public String getTmpMatFileName() {
        return getTmpFileName(outDir, "mat");
    }
}