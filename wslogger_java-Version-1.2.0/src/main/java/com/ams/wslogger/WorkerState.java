package com.ams.wslogger;

/**
 * Enumeration to indicate the state of the worker thread.
 */
public enum WorkerState {
    Starting, Started, Idle, PreparingForNewJob, Prepared, Working, StopRequested, Stopping, Stopped, SocketError,
}