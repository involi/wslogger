package com.ams.wslogger;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseWriter extends Thread {

    protected long curIndex = 0;
    protected OutputNameFactory nameFactory = null;
    protected Queue<WSData> dataQueue = new LinkedBlockingQueue<WSData>();
    protected boolean noMoreData = false;
    protected Logger logger = Logger.getLogger(BaseWriter.class.getName());

    public void run() {
        // let the child to open file handle
        if (!openFile()) {
            logger.log(Level.SEVERE, "Could not open temporary file to write!");
            closeFile(); // can't write if file open failed
            removeTemporaryFile(); // remove temporary file
            return;
        }

        // this loop will write out all the data strings from the queue,
        // even after no more data flag is set.
        while (!noMoreData || !dataQueue.isEmpty()) {
            // get one data string and write it to the disk
            WSData data = dataQueue.peek();
            if (data != null) {
                if (data.data != null && data.data.isEmpty()) { // no need to write if no data
                    dataQueue.remove();
                    continue;
                }
                curIndex++;
                writeData(data, curIndex);

                // remove the element
                dataQueue.remove();
            }

            // give it a break
            try {
                Thread.sleep(10); // reduces CPU load
            } catch (Exception ex) {
                logger.log(Level.WARNING, ex.getMessage());
            }
        }

        // allow the child to close any file handle
        removeTemporaryFile(); // remove temporary file
        closeFile();
    }

    /**
     * Push new data string to the queue
     * 
     * @param data data to be written
     */
    public void push(WSData data) {
        dataQueue.add(data);
    }

    /**
     * This method will be called when a new data string is to be written to the
     * disk.
     * 
     * @param data  data to be written
     * @param index data index
     */
    public abstract void writeData(WSData data, long index);

    /**
     * Method to allow the child class implement a file opening functionality. It is
     * the child's responsibility to implement the close file functionality along
     * with it. If this method returns <code>false</code> then the file closing
     * method will be called.
     * 
     * @return <code>true</code> if file is opened successfully.
     */
    public abstract boolean openFile();

    /**
     * Close any opened file handle.
     */
    public abstract void closeFile();

    /**
     * Allows child implementation to remove temporary files.
     */
    public abstract void removeTemporaryFile();

    /**
     * Signal the writer that there will be no more data.
     * 
     * @param noMoreData the noMoreData to set
     */
    public void setNoMoreData(boolean noMoreData) {
        this.noMoreData = noMoreData;
    }

    /**
     * Set the output name factory.
     * 
     * @param nameFactory the nameFactory to set
     */
    public void setNameFactory(OutputNameFactory nameFactory) {
        this.nameFactory = nameFactory;
    }
}