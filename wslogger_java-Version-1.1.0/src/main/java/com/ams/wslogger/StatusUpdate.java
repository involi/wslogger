package com.ams.wslogger;

public class StatusUpdate {

    public WorkerState state;
    public int progress;
    public String message;

    /**
     * Construct a new status update instance.
     * 
     * @param wState worker state
     * @param pValue progress value
     * @param msg    message string
     */
    public StatusUpdate(WorkerState wState, int pValue, String msg) {
        state = wState;
        progress = pValue;
        message = msg;
    }
}