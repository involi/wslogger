package com.ams.wslogger;

import java.util.List;
import java.util.logging.Level;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class CsvWriter extends BaseWriter {
    private List<String> headers = new ArrayList<>();
    private ObjectMapper oMapper = new ObjectMapper();
    private Date startTime = new Date();
    private StopWatch stopWatch = new StopWatch();
    private static final long LAP_DURATION = 60 * 15; // seconds * minutes
    private static String NEWLINE = System.getProperty("line.separator");

    /**
     * Overrides the setting up of name factory functionality and intializes the
     * start time from it.
     */
    @Override
    public void setNameFactory(OutputNameFactory nameFactory) {
        super.setNameFactory(nameFactory);

        // initialize the startTime at the beginning
        startTime = nameFactory.getStart();
        stopWatch.start();
    }

    @Override
    public boolean openFile() {
        try {
            Path p = Paths.get(nameFactory.getTmpCsvFileName());
            if (Files.exists(p, LinkOption.NOFOLLOW_LINKS)) {
                // must delete any previous temporary file first
                Files.delete(p);
            }
            Files.createFile(p);
            return true;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
        return false;
    }

    @Override
    public void closeFile() {
        String tmpFilePath = nameFactory.getTmpCsvFileName();
        Path p = Paths.get(tmpFilePath);
        if (Files.exists(p, LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.delete(p);
                // sleep(2000); // let the OS finish up writing data
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }
    }

    /**
     * This method will recreate the output file with the content of temporary file
     * and add headers at the beginning and finally delete the temporary file.
     * 
     * @param startDate sets the starting date instance to be used in the file name
     *                  factory
     * @param endDate   same as the startDate argument
     */
    public void removeTemporaryFile(Date startDate, Date endDate) {
        File tmpFile = new File(nameFactory.getTmpCsvFileName());
        if (!tmpFile.exists()) { // nothing to remove or rewrite
            return;
        }

        if (tmpFile.length() > 0) { // only create new file if theres something to write
            String newFilePath = nameFactory.getCsvOutputFileName(startTime, endDate);
            try {
                BufferedWriter newFileWriter = new BufferedWriter(new FileWriter(newFilePath));

                // write the header
                String header = "";
                for (String hd : headers) {
                    header += hd + ",";
                }
                newFileWriter.write(header);
                newFileWriter.newLine();

                // read and write temporary data set
                BufferedReader tmpReader = new BufferedReader(new FileReader(nameFactory.getTmpCsvFileName()));
                for (String line; (line = tmpReader.readLine()) != null;) {
                    line = line.trim();
                    newFileWriter.write(line);
                    newFileWriter.newLine();
                }

                // close the file handles
                newFileWriter.flush();
                newFileWriter.close();
                tmpReader.close();

                // wait to finish
                sleep(1000);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }

        // delete the temporary file
        try {
            Files.deleteIfExists(Paths.get(nameFactory.getTmpCsvFileName()));
        } catch (Exception ex) {
            logger.log(Level.WARNING, ex.getMessage());
        }
    }

    /**
     * Method to remove the temporary file by writing out the output file with its
     * contents.
     */
    @Override
    public void removeTemporaryFile() {
        File tmpFile = new File(nameFactory.getTmpCsvFileName());
        if (!tmpFile.exists()) { // nothing to remove or rewrite
            return;
        }

        if (tmpFile.length() > 0) { // only create new file if theres something to write
            String newFilePath = nameFactory.getCsvOutputFileName(startTime, nameFactory.getEnd());
            try {
                BufferedWriter newFileWriter = new BufferedWriter(new FileWriter(newFilePath));

                // write the header
                String header = "";
                for (String hd : headers) {
                    header += hd + ",";
                }
                newFileWriter.write(header);
                newFileWriter.newLine();

                // read and write temporary data set
                BufferedReader tmpReader = new BufferedReader(new FileReader(nameFactory.getTmpCsvFileName()));
                for (String line; (line = tmpReader.readLine()) != null;) {
                    line = line.trim();
                    newFileWriter.write(line);
                    newFileWriter.newLine();
                }

                // close the file handles
                newFileWriter.flush();
                newFileWriter.close();
                tmpReader.close();

                // wait to finish
                sleep(1000);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }

        // delete the temporary file
        try {
            Files.deleteIfExists(Paths.get(nameFactory.getTmpCsvFileName()));
        } catch (Exception ex) {
            logger.log(Level.WARNING, ex.getMessage());
        }
    }

    @Override
    public void writeData(WSData wsData, long index) {
        try {
            if (headers.isEmpty()) { // add local timestamp header once
                headers.add("timestamp_local");
            }
            JsonNode node = oMapper.readTree(wsData.data);
            JsonNode aircraft = node.get("aircraft");
            if (aircraft.isArray()) { // work through the array
                Iterator<JsonNode> iterator = aircraft.elements();
                Path tmpFilePath = Paths.get(nameFactory.getTmpCsvFileName());
                String dataLine = "";
                while (iterator.hasNext()) {
                    JsonNode objNode = iterator.next();

                    // 1. construct/edit the headers
                    Iterator<String> fields = objNode.fieldNames();
                    while (fields.hasNext()) {
                        String field = fields.next();
                        boolean found = false;
                        for (String header : headers) {
                            if (header.equals(field)) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            headers.add(field);
                        }
                    }

                    // 2. construct the csv data line
                    for (String key : headers) {
                        if (key.equals("timestamp_local")) {
                            dataLine += "\"" + wsData.timestampLocalStr + "\",";
                            continue;
                        }
                        JsonNode value = objNode.get(key);
                        if (value == null) { // if field is not there add empty column
                            dataLine += ",";
                            continue;
                        }
                        if (value.isNumber()) {
                            dataLine += Double.toString(value.asDouble(0.0)) + ",";
                        } else {
                            dataLine += value.toString() + ",";
                        }
                    }

                    // 3. add a new line
                    dataLine += NEWLINE;
                }

                // write to the disk
                Files.write(tmpFilePath, dataLine.getBytes(), StandardOpenOption.APPEND);
            }

            // check if it is time to make a new output
            // file with the contents of the temporary file
            if (stopWatch.timeSpent() > LAP_DURATION) {
                Date endTime = new Date();

                // copy temporary, reopen temporary
                removeTemporaryFile(startTime, endTime);
                closeFile();
                openFile();

                // reset the stopwatch
                stopWatch.stop();
                stopWatch.start();

                // swap the end time and start time
                startTime = endTime;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}