package com.ams.wslogger;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

/**
 * Shorthand class to get a user selected output directory using file chooser
 * dialog.
 */
class DirectoryChooser {
    /**
     * Get a output directory from user using file dialogue.
     * 
     * @param parent Parent component
     * @param title  dialog title
     * @param curDir initial directory
     * @return if selected absolute path to the directory else <code>null</code>
     */
    public static String getOutputDirectory(Component parent, String title, String curDir) {
        String rv = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(curDir));
        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            rv = chooser.getSelectedFile().getAbsolutePath();
        }
        return rv;
    }
}