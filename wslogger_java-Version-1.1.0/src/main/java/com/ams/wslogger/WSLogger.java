package com.ams.wslogger;

import java.awt.EventQueue;
import java.io.InputStream;
import java.util.logging.LogManager;

import javax.swing.UIManager;

public class WSLogger {

    /**
     * Main entry point. If ran from command prompt using <code>java -jar</code>
     * command this will be executed.
     * 
     * @param args command line arguments
     */
    public static void main(String[] args) {
        WSLogger logger = new WSLogger();
        logger.runApp();
    }

    /**
     * Schedule to run the GUI with event queue and set the users home directory as
     * initial output directory.
     */
    public void runApp() {
        runApp(null);
    }

    /**
     * Schedule to run the GUI with the given <code>workingPath</code> set as the
     * initial output directory.
     * 
     * @param workingPath path string
     */
    public void runApp(String workingPath) {
        // try to setup native look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Error setting look and feel: " + e.getMessage());
        }

        // setup logger configuration
        try {
            InputStream configStream = getClass().getResourceAsStream("/configs/logging.properties");
            LogManager.getLogManager().readConfiguration(configStream);
        } catch (Exception ex) {
            System.out.println("Could not setup logging: " + ex.getMessage());
        }

        EventQueue.invokeLater(() -> {
            MainGui gui = new MainGui(workingPath);
            gui.setVisible(true);
        });
    }
}