package com.ams.wslogger;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * WSData store single response data with additional fields.
 */
public class WSData {
    public Date timestampLocal;
    public String data;
    public String timestampLocalStr;

    /**
     * Construct a data instance with given info.
     * 
     * @param now  local date time stamp
     * @param data string data
     */
    public WSData(Date now, String data) {
        timestampLocal = now;
        this.data = data;
        timestampLocalStr = getIsoTimestamp();
    }

    /**
     * Get the ISO formated date-time string from the <code>timestampLocal</code>.
     * 
     * @return string representation of local time
     */
    private String getIsoTimestamp() {
        LocalDateTime lTime = timestampLocal.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return lTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
}