package com.ams.wslogger;

import org.java_websocket.handshake.ServerHandshake;

public interface WSListener {

    /**
     * Will be called on connection open event.
     * 
     * @param handshake server handshake object
     */
    void onOpen(ServerHandshake handshake);

    /**
     * Will be called on message arrival
     * 
     * @param message server sent message
     */
    void onMessage(String message);

    /**
     * Will be called upon closing of the connection
     * 
     * @param code   closing code
     * @param reason reason message
     * @param remote party who closed the connection
     */
    void onClose(int code, String reason, boolean remote);

    /**
     * Will be called upon any exception event.
     * 
     * @param ex <code>Exception</code> object
     */
    void onError(Exception ex);
}