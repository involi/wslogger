package com.ams.wslogger;

import java.util.List;
import java.util.logging.Level;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;

public class CsvWriter extends BaseWriter {
    private List<String> headers = new ArrayList<>();
    private BufferedWriter fileWriter = null;
    private ObjectMapper oMapper = new ObjectMapper();

    @Override
    public boolean openFile() {
        try {
            String tmpFilePath = nameFactory.getTmpCsvFileName();
            fileWriter = new BufferedWriter(new FileWriter(tmpFilePath));
            return true;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            fileWriter = null;
        }
        return false;
    }

    @Override
    public void closeFile() {
        if (fileWriter != null) {
            try {
                fileWriter.flush();
                fileWriter.close();
                sleep(2000);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }
    }

    /**
     * This method will recreate the output file with the content of temporary file
     * and add headers at the beginning and finally delete the temporary file.
     */
    @Override
    public void removeTemporaryFile() {
        File tmpFile = new File(nameFactory.getTmpCsvFileName());
        if (!tmpFile.exists()) { // nothing to remove or rewrite
            return;
        }

        if (tmpFile.length() > 0) { // only create new file if theres something to write
            String newFilePath = nameFactory.getCsvOutputFileName(nameFactory.getStart(), nameFactory.getEnd());
            try {
                BufferedWriter newFileWriter = new BufferedWriter(new FileWriter(newFilePath));

                // write the header
                String header = "";
                for (String hd : headers) {
                    header += hd + ",";
                }
                newFileWriter.write(header);
                newFileWriter.newLine();

                // read and write temporary data set
                BufferedReader tmpReader = new BufferedReader(new FileReader(nameFactory.getTmpCsvFileName()));
                for (String line; (line = tmpReader.readLine()) != null;) {
                    line = line.trim();
                    newFileWriter.write(line);
                    newFileWriter.newLine();
                }

                // close the file handles
                newFileWriter.flush();
                newFileWriter.close();
                tmpReader.close();

                // wait to finish
                sleep(1000);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage());
            }
        }

        // delete the temporary file
        try {
            Files.deleteIfExists(Paths.get(nameFactory.getTmpCsvFileName()));
        } catch (Exception ex) {
            logger.log(Level.WARNING, ex.getMessage());
        }
    }

    @Override
    public void writeData(WSData wsData, long index) {
        try {
            if (headers.isEmpty()) { // add local timestamp header once
                headers.add("timestamp_local");
            }
            JsonNode node = oMapper.readTree(wsData.data);
            JsonNode aircraft = node.get("aircraft");
            if (aircraft.isArray()) { // work through the array
                Iterator<JsonNode> iterator = aircraft.elements();
                while (iterator.hasNext()) {
                    JsonNode objNode = iterator.next();

                    // 1. construct/edit the headers
                    Iterator<String> fields = objNode.fieldNames();
                    while (fields.hasNext()) {
                        String field = fields.next();
                        boolean found = false;
                        for (String header : headers) {
                            if (header.equals(field)) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            headers.add(field);
                        }
                    }

                    // 2. construct the csv data line
                    String dataLine = "";
                    for (String key : headers) {
                        if (key.equals("timestamp_local")) {
                            dataLine += "\"" + wsData.timestampLocalStr + "\",";
                            continue;
                        }
                        JsonNode value = objNode.get(key);
                        if (value == null) { // if field is not there add empty column
                            dataLine += ",";
                            continue;
                        }
                        if (value.isNumber()) {
                            dataLine += Double.toString(value.asDouble(0.0)) + ",";
                        } else {
                            dataLine += value.toString() + ",";
                        }
                    }

                    // 3. write to the file
                    fileWriter.write(dataLine);
                    fileWriter.newLine();
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}