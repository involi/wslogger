package com.ams.wslogger;

import java.util.concurrent.TimeUnit;

/**
 * A simple second counting watch.
 */
public class StopWatch {

    private long startTime = 0;
    private boolean started = false;

    /**
     * Start the watch.
     */
    public void start() {
        if (!started) { // only start when its not already started
            startTime = System.nanoTime();
            started = true;
        }
    }

    /**
     * Stop the watch.
     */
    public void stop() { // stop the watch anyway
        reset();
    }

    /**
     * Resets the watch to initial not started state.
     */
    public void reset() {
        started = false;
        startTime = 0;
    }

    /**
     * Get the amount of time in seconds since the watch was started.
     * 
     * @return seconds elapsed
     */
    public long timeSpent() {
        if (started) {
            return TimeUnit.SECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS);
        }
        return 0;
    }
}