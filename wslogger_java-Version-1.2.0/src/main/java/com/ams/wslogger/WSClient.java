package com.ams.wslogger;

import java.net.URI;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

/**
 * WSClient
 */
public class WSClient extends WebSocketClient {

    private static String URL = "wss://data.involi.live/skyguide/ws/v3/";
    private WSListener listener = null;

    public WSClient() throws Exception {
        super(new URI(WSClient.URL));
    }

    /**
     * Set the websocket server url
     * 
     * @param url server url
     */
    public static void setURL(String url) {
        url = url.trim();
        if (!url.isEmpty()) {
            WSClient.URL = url;
        }
    }

    /**
     * Get the internal websocket url
     * 
     * @return the URL
     */
    public static String getURL() {
        return WSClient.URL;
    }

    /**
     * @param listener the listener to set
     */
    public void setListener(WSListener listener) {
        this.listener = listener;
    }

    /**
     * @return the listener
     */
    public WSListener getListener() {
        return listener;
    }

    @Override
    public void onOpen(ServerHandshake handshake) {
        if (listener != null) {
            listener.onOpen(handshake);
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        if (listener != null) {
            listener.onClose(code, reason, remote);
        }
    }

    @Override
    public void onError(Exception ex) {
        if (listener != null) {
            listener.onError(ex);
        }
    }

    @Override
    public void onMessage(String messsage) {
        if (listener != null) {
            listener.onMessage(messsage);
        }
    }
}